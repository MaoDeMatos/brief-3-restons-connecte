package com.example.app.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.example.app.model.Country;
import com.example.app.repository.ApiRepository;
import org.springframework.http.HttpStatus;

import lombok.Data;

@Data
@Service
public class CountryService {

  @Autowired
  private ApiRepository apiRepository;

  public Iterable<Country> getCountries() {
    return apiRepository.findAll();
  }

  public Country getCountry(final Long id) {
    Optional<Country> country = apiRepository.findById(id);

    if (country.isPresent()) {
      return country.get();
    } else {
      Country error = new Country();
      error.setName("Impossible de trouver la ressource demandée (n°" + id + ")");
      error.setCapitalCity(String.valueOf(HttpStatus.NOT_FOUND.value()));

      return error;
    }
  }

  public void deleteCountry(final Long id) {
    apiRepository.deleteById(id);
  }

  /**
   * Create a new country
   * 
   * @param country
   * @return
   */
  public Country saveCountry(Country country) {
    // Uppercase country code
    country.setCode(country.getCode().toUpperCase());
    // Capitalize country & capital city names
    String name = country.getName();
    country.setName(capitalizeString(name));

    String capitalCity = country.getCapitalCity();
    country.setCapitalCity(capitalizeString(capitalCity));

    Country savedCountry = apiRepository.save(country);

    return savedCountry;
  }

  /**
   * Update a country
   * 
   * @param country
   * @param id
   * @return
   */
  public Country saveCountry(Country country, Long id) {
    Optional<Country> c = apiRepository.findById(id);

    if (c.isPresent()) {
      Country currentCountry = c.get();

      String code = country.getCode();
      if (code != null) {
        // Uppercase country code
        currentCountry.setCode(code.toUpperCase());
      }
      String name = country.getName();
      if (name != null) {
        // Capitalize country name
        currentCountry.setName(capitalizeString(name));
      }
      String capitalCity = country.getCapitalCity();
      if (capitalCity != null) {
        // Capitalize capital city name
        currentCountry.setCapitalCity(capitalizeString(capitalCity));
      }

      apiRepository.save(currentCountry);

      return currentCountry;
    } else {
      Country error = new Country();
      error.setName("Impossible de mettre à jour la ressource demandée (n°" + id + ")");
      error.setCapitalCity("Error");

      return error;
    }
  }

  /**
   * Returns first letter to upper case
   * 
   * @param str
   * @return
   */
  private String capitalizeString(String str) {
    return str.substring(0, 1)
        .toUpperCase()
        + str.substring(1);
  }

}
