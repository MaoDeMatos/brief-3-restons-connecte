package com.example.app.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.example.app.model.Country;

@Repository
public interface ApiRepository extends CrudRepository<Country, Long> {

}
