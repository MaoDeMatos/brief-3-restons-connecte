package com.example.app.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.app.model.Country;
import com.example.app.service.CountryService;

@CrossOrigin(originPatterns = "*") // CORS policy
@RestController
public class CountryController {

  @Autowired
  private CountryService countryService;

  /**
   * Read - Get all countries
   * 
   * @return - An Iterable object of Country full filled
   */
  @GetMapping("/countries")
  public Iterable<Country> getCountries() {
    return countryService.getCountries();
  }

  /**
   * Read - Get one country
   * 
   * @param id The id of the country
   * @return An Country object full filled
   */
  @GetMapping("/country/{id}")
  public Country getCountry(@PathVariable("id") final Long id) {
    Country country = countryService.getCountry(id);

    return country;
  }

  /**
   * Delete - Delete a country
   * 
   * @param id - The id of the country to delete
   */
  @DeleteMapping("/country/{id}")
  public void deleteCountry(@PathVariable("id") final Long id) {
    countryService.deleteCountry(id);
  }

  /**
   * Create - Add a new country
   * 
   * @param country An object country
   * @return The country object saved
   */
  @PostMapping("/country")
  public Country createCountry(@RequestBody Country country) {
    return countryService.saveCountry(country);
  }

  /**
   * Update - Update an existing country
   * 
   * @param id      - The id of the country to update
   * @param country - The country object updated
   * @return
   */
  @PutMapping("/country/{id}")
  public Country updateCountry(@PathVariable("id") final Long id, @RequestBody Country country) {
    return countryService.saveCountry(country, id);
  }

}
