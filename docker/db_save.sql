-- Adminer 4.8.1 MySQL 8.0.27 dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

SET NAMES utf8mb4;

DROP TABLE IF EXISTS `countries`;
CREATE TABLE `countries` (
  `id` int NOT NULL AUTO_INCREMENT,
  `code` varchar(2) NOT NULL,
  `name` varchar(255) NOT NULL,
  `capital_city` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

INSERT INTO `countries` (`id`, `code`, `name`, `capital_city`) VALUES
(1,	'FR',	'France',	'Paris'),
(2,	'UK',	'Royaume-Uni',	'Londres'),
(3,	'RU',	'Russie',	'Moscou'),
(4,	'MA',	'Maroc',	'Rabat'),
(5,	'DE',	'Allemagne',	'Berlin'),
(6,	'ES',	'Espagne',	'Madrid'),
(7,	'US',	'États-Unis',	'Washington'),
(8,	'IT',	'Italie',	'Rome'),
(9,	'GR',	'Grèce',	'Athènes'),
(10,	'BR',	'Brésil',	'Rio de Janeiro');

-- 2021-12-21 09:46:18