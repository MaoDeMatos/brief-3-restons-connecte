# API building image
FROM openjdk:11.0.13-jdk-slim as api-base

WORKDIR /app

COPY  ./api/pom.xml .
COPY  ./api/mvnw    .
COPY  ./api/.mvn    ./.mvn

# Ensures the file is executable as a linux script
RUN apt update && apt install dos2unix \
  && dos2unix ./mvnw

# Dependencies caching
RUN ./mvnw dependency:go-offline -B

COPY ./api/src      src

RUN ./mvnw package

# Final API image
FROM openjdk:11.0.13-jdk-slim as api

COPY --from=api-base    ./app/target/api*.jar    /app/api.jar

# # # # # # # # # # # # # # # # # # # # #

# MySQL image
FROM mysql:8.0.27 as mysql_db

# DB init file
# /!\ WILL BE WRITTEN TO THE VOLUME /!\
# Remove the volume to reset the Database
COPY ./docker/db_save.sql /docker-entrypoint-initdb.d/db_save.sql

# # # # # # # # # # # # # # # # # # # # #

# Frontend building image
FROM node:14.18.2-alpine as frontend-base

WORKDIR /app

COPY ./front/package.json .

RUN npm install

COPY ./front .

RUN npm run build

# Frontend final image
FROM nginx:1.20.2-alpine as frontend

COPY ./docker/nginx.conf    /etc/nginx/conf.d/default.conf
COPY --from=frontend-base   /app/dist /app
