import Vue from "vue";
import VueRouter from "vue-router";
import Home from "../views/Home.vue";
import EditCountry from "../components/EditCountry.vue";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    component: Home,
  },
  {
    path: "/edit/:id",
    component: EditCountry,
  },
];

const router = new VueRouter({
  mode: "history",
  routes,
});

export default router;
