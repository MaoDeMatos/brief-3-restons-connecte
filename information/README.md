# Screenshots

`FRONTEND`

GET all & POST

![Before POST](./resources/before_POST.png)

![After POST](./resources/after_POST.png)

GET one by id & PUT by id

![Before POST](./resources/form_PUT.png)

`cURL`

GET one

![cURL GET](./resources/cURL_GET.png)

GET custom error

![cURL GET custom error](./resources/cURL_GET_error.png)
