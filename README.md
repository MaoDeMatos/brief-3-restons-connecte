# Screenshots

See the screenshots [here](./information/README.md).

# Dependencies

- Java 11
- Node.js <= 14

You can build locally and use the app without it, but it is meant to be used with Docker, and it will be easier to use with it.

# Stack

## Docker containers

The API is running with two core containers :

- `OpenJDK 11 slim` for the API
- `MySQL 8` for the database

The frontend app is running in an `Nginx` container.

<br />

There's another container for adminer but it is obviously optional and disabled by default.

If you need it just uncomment the adminer block in the `docker-compose` file.

## Database configuration

The first initialization of the database can delay the time when the api will be ready.

You can find it in the `docker-compose.yml` file :

```yml
MYSQL_RANDOM_ROOT_PASSWORD: "yes"
MYSQL_DATABASE: spring_api
MYSQL_USER: api_admin
MYSQL_PASSWORD: api_pass
```

# Usage

## Docker (recommanded)

```
Commands must be executed in the root dir
```

Start :

- `docker-compose up -d` to run the DB, api and frontend app
- use the option `--build` if you made changes and they are not copied to the container with the above command

Stop :

- `docker-compose stop` to stop it
- `docker-compose down` to completely remove it

## Manually start

Build the frontend locally :

```sh
# In the root dir
cd ./front && npm install && npm run build
```

Start the frontend locally with `npm run serve`

Build the backend locally :

```sh
# In the root dir
cd ./api && ./mvnw clean package
```

```
You need a database ready to start the API
```

Start the backend locally with `java -jar ./api/target/api-0.0.1-SNAPSHOT.jar`

# Keep the database container running !

This way the apps will be able to use the database container even if started locally !

# API

## Endpoints

- `GET /countries` => Get all countries
- `GET /country/{id}` => Get a country by ID
- `POST /country` => Save a country to the DB
- `PUT /country/{id}` => Update a country by ID
- `DELETE /country/{id}` => Delete a country by ID

## cURL requests

GET /countries (All)

```sh
curl http://localhost:9000/countries
```

GET /country/{id}

```sh
curl http://localhost:9000/country/22
```

POST /country/

```sh
curl -d '{"name":"France","code":"FR","capitalCity":"Paris"}' -H "Content-Type: application/json" http://localhost:9000/country
```

PUT /country/{id}

```sh
curl -d '{"name":"France","code":"FR","capitalCity":"Paris"}' -H "Content-Type: application/json" -X PUT http://localhost:9000/country/1
```

DELETE /country/{id}

```sh
curl -X DELETE http://localhost:9000/country/3
```

# Caveats

The frontend app works well in most cases, but we couldn't figure out quickly enough how to properly fetch and display asynchronously the data of the GET ALL after sending the form.

- When launching the app : load data :white_check_mark:
- When POSTing a new resource : :negative_squared_cross_mark: (page reloads)
- When PUTting data to update a resource (by id) : :negative_squared_cross_mark: (you need to click on the "refresh button")
- When DELETing a new resource : :negative_squared_cross_mark: (page reloads)
